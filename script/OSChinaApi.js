var API_HOST = 'http://www.oschina.net/';

var OpenAPI = {
	"news_list":  			API_HOST + "action/api/news_list?catalog=1&pageSize=20",
	"news_hot":  			API_HOST + "action/api/news_list?show=week",
	"blog_list":  			API_HOST + "action/api/blog_list?type=latest&pageSize=20",
	"blog_recommend":  		API_HOST + "action/api/blog_list?type=recommend&pageSize=20",
	"post_list":  			API_HOST + "action/api/post_list?pageSize=20",
	"tweet_list":			API_HOST + "action/api/tweet_list?pageSize=20",
	
	"news_detail":			API_HOST + "action/api/news_detail",
	"blog_detail":			API_HOST + "action/api/blog_detail",
	"post_detail":			API_HOST + "action/api/post_detail",
	"software_detail":		API_HOST + "action/api/software_detail",
	"tweet_detail":			API_HOST + "action/api/tweet_detail",
	
	"favorite_add":			API_HOST + "action/api/favorite_add",
	"favorite_delete":		API_HOST + "action/api/favorite_delete",
	
	"tweet_like":			API_HOST + "action/api/tweet_like",
	"tweet_unlike":			API_HOST + "action/api/tweet_unlike",
	
	"blogcomment_list":		API_HOST + "action/api/blogcomment_list?pageSize=20",
	"comment_list":			API_HOST + "action/api/comment_list?pageSize=20",
	"software_tweet_list":	API_HOST + "action/api/software_tweet_list?pageSize=20",
	
	"comment_pub":			API_HOST + "action/api/comment_pub",
	"comment_reply":		API_HOST + "action/api/comment_reply",
	"blogcomment_pub":		API_HOST + "action/api/blogcomment_pub",
	"tweet_pub":			API_HOST + "action/api/tweet_pub",

	"search_list":			API_HOST + "action/api/search_list?pageSize=20",
	
	"active_list":			API_HOST + "action/api/active_list?pageSize=20",
	"message_list":			API_HOST + "action/api/message_list?pageSize=20",
	"friends_list":			API_HOST + "action/api/friends_list?pageSize=20",
	"my_tweet_like_list":	API_HOST + "action/api/my_tweet_like_list?pageSize=20",
	"notice_clear":			API_HOST + "action/api/notice_clear",
	
	"login":  				API_HOST + "action/api/login_validate",
	"logout":  				API_HOST + "action/user/logout",
	"my_information":  		API_HOST + "action/api/my_information",
	"user_information":  	API_HOST + "action/api/user_information?pageSize=20",
	"user_notice":  		API_HOST + "action/api/user_notice",
	"community_report":  	API_HOST + "action/communityManage/report",
};